
#Change dir 
#setwd('F:\projects\studier\rwork\')
#directory <- ("F:\projects\studier\rwork\")


#uM <- scan("uM.txt")
uM <- matrix(c(scan("uM.txt")), nrow = 3, ncol = 3, byrow = TRUE)

#uW <- scan("uW.txt")
uW <- matrix(c(scan("uW.txt")), nrow = 3, ncol = 3, byrow = TRUE)

matching = galeShapley.marriageMarket(uM, uW)

matching

#Stable matching?
galeShapley.checkStability(uM, uW, matching$proposals, matching$engagements)

