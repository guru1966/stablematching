library(matchingR)

# set seed
set.seed(1)
# set number of men
nmen = 2500
# set number of women
nwomen = 2000

# generate preferences
uM = matrix(runif(nmen*nwomen), nrow = nwomen, ncol = nmen)
uW = matrix(runif(nmen*nwomen), nrow = nmen, ncol = nwomen)


# male-optimal matching
resultsM = galeShapley.marriageMarket(uM, uW)
str(resultsM)

resultsM$proposals

galeShapley.checkStability(uM, uW, resultsM$proposals, resultsM$engagements)

# female-optimal matching
resultsW = galeShapley.marriageMarket(uW, uM)
str(resultsW)

galeShapley.checkStability(uW, uM, resultsW$proposals, resultsW$engagements)

# Example: College Admissions Problem
# set seed
set.seed(1)
# set number of students
nstudents = 1000
# set number of colleges
ncolleges = 400
# generate preferences
uStudents = matrix(runif(ncolleges*nstudents), nrow = ncolleges, ncol = nstudents)
uColleges = matrix(runif(nstudents*ncolleges), nrow = nstudents, ncol = ncolleges)
# student-optimal matching
results = galeShapley.collegeAdmissions(studentUtils =  uStudents, collegeUtils =  uColleges, slots = 2)
str(results)

# check if matching is stable
galeShapley.checkStability(uStudents, uColleges, results$matched.students, results$matched.colleges)