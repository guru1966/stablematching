library(matchingR)

uM = matrix(c(1.0, 0.5, 0.0,
              0.5, 0.0, 0.5,
              0.0, 1.0, 1.0), nrow = 3, ncol = 3, byrow = TRUE)

uW = matrix(c(0.0, 1.0, 0.0,
              0.5, 0.0, 0.5,
              1.0, 0.5, 1.0), nrow = 3, ncol = 3, byrow = TRUE)

prefM = matrix(c(1, 2, 3,
                 2, 3, 2,
                 3, 1, 1), nrow = 3, ncol = 3, byrow = TRUE)

prefW = matrix(c(3, 1, 3,
                 2, 3, 2,
                 1, 2, 1), nrow = 3, ncol = 3, byrow = TRUE)

matching = galeShapley.marriageMarket(uM, uW)

matching

matching2 = galeShapley.marriageMarket(proposerPref = prefM, reviewerPref = prefW)

matching2

matching2 = galeShapley.marriageMarket(proposerPref = prefW, reviewerPref = prefM)

matching2

matching2$proposals
#Stable ?
galeShapley.checkStability(uM, uW, matching$proposals, matching$engagements)